<!DOCTYPE html>
<html>
<head>
    <title>BBS Homepage</title>
    <meta charset="UTF-8">
    <style>
        body {
            background-color: #DCDCDC;
            font-family: "Arial";
            font-size: 15px;
        }
        div.logbox {
            width: 100%;
            height: 60%;
            margin: 0px auto;
            border: 0px solid #;
            background-color: #FFFFFF;
        }
        div.innerbox h1 {
            background-color: #ADD8E6;
            padding: 10px;
            font-family: "Arial";
            font-weight: normal;
            color: black;
            border: 1px solid #98b9d0;
        }
        div fieldset {
            margin: 30px;
            border: 1px solid #98b9d0;
        }
    </style>
</head>
<body>
    <hr>
    <div class ="logbox">
        <div class = "innerbox">
        <h1>Welcome to BBS</h1>
<?php
session_start();
#show add story option if a user is logged in
if (!isset($_SESSION['username'])){
?>
        <fieldset>
            <legend><label for="usertext">Login / Register</label></legend>
            <form id="loginform" action="login.php" method="POST">
                <br/>
                Username:
                <input type="text" name="username" id="usertext" />&nbsp;&nbsp;&nbsp;&nbsp;
                Password:
                <input type="text" name="password" id="passtext" /><br/>
                <br/>
                <input type="submit" name="login" value="Login" />
                <input type="submit" name="register" value="Register" />
            </form>
        </fieldset>
<?php
} else {
	$_SESSION['token'] = substr(md5(rand()), 0, 10); // generate a 10-character random string
?>
        <fieldset>
            <form id="logout" action="logout.php" method="POST">
                Welcome back, <?php echo $_SESSION['username']; ?>
                <input type="submit" name="logout" value="Logout" />
            </form>
            <br/><br/>
            <form id="sc" action="view_sc.php" method="POST">
                <input type="submit" name="view" value="View Previoius Stories/Comments" />
            </form>
            <br/><br/>
            <form id="sc" action="add_story.php" method="POST">
                <input type="submit" name="add_story" value="Write new story" />
				<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            </form>
        </fieldset>
<?php
}
?>
        </div>
    </div>
    <div class ="logbox">
<?php

$mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');

if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

$stmt = $mysqli->prepare("select * from stories order by datetime desc");

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$result = $stmt->get_result();

while ($story = $result->fetch_assoc()){
    $_SESSION['s_id'] = $story['story_id']);
    $_SESSION['s_category'] = $story['category']);
	$_SESSION['s_title'] = $story['s_title']);
	$_SESSION['s_datetime']= $story['datetime']);
?>
    <div class="innerbox">
        <fieldset>
            <legend><label for="usertext">Category:<?php echo $_SESSION['s_category']; ?></label></legend>
            <form action='read_story.php' method='GET'>
                <?php echo $_SESSION['s_title']; ?>
                <input type="submit" name="read" value="Read" />
                <input type='hidden' value="<?php echo $_SESSION['s_id']; ?>" name="story_id">
            </form>
        </fieldset>
    </div>
<?php
}
$stmt->close();
?>
    </div>
</body>
</html>
