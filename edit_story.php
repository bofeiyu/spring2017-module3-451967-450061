<!DOCTYPE html>
<html>
<head>
	<title>Edit Story</title>
    <meta charset="UTF-8">
</head>
<body>

<?php
session_start();

if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

if (!isset($_SESSION['username'])){
    echo "Login first...";
    echo "Redirect to homepage in 1 seconds...";
    header("Refresh:1; url = home.php");
    exit;
}

$mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');

if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

$_SESSION['story_id'] = (string) trim($_POST['story_id']);

$sql = "select * from stories where story_id=?";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param('i',$_SESSION['story_id']);

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$result = $stmt->get_result();
$stmt->close();
$row = $result->fetch_assoc();
$_SESSION['s_category'] = $row['category']);
$_SESSION['s_title'] = $row['s_title']);
$_SESSION['s_content'] = $row['s_content']);
$_SESSION['s_link'] = $row['link'];
?>

<form id="st" method="POST" action="update_story.php" >
    <label for="s_t">Story title: </label><br>
    <textarea name="s_title" rows="2" cols="100" id="s_t"> <?php echo $_SESSION['s_title']; ?></textarea>
    <br>
    <label for="url">Link: </label><br>
    <textarea name="s_link" rows="2" cols="100" id="url"> <?php echo $_SESSION['s_link']; ?></textarea>
    <br>
    <label for="s_cate">Category: </label><br>
    <input type="text" name="s_category" id="s_cate" />
    <br>
    <label for="s_c">Story content: </label><br>
    <textarea name="s_content" rows="20" cols="100" id="s_c"> <?php echo $_SESSION['s_content']; ?></textarea>
    <br>
	<input type="submit" name="submit" value="Submit" />
    <input type="submit" name="discard" value="Discard" />
</form>

</body>
</html>
