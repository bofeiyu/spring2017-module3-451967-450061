<!DOCTYPE html>
<html>
<head>
	<title>Delete Comment</title>
    <meta charset="UTF-8">
</head>
<body>

<?php
session_start();

if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

if (!isset($_SESSION['username'])){
    echo "Login first...";
    echo "Redirect to homepage in 1 seconds...";
    header("Refresh:1; url = home.php");
    exit;
}

$mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');

if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

$c_id =  (string) trim($_POST['comment_id']);

$sql = "delete from comments where comment_id=?";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param('i',$c_id);

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$result = $stmt->get_result();

unset($_SESSION['comment_id']);
$stmt->close();
header("Location: view_sc.php");

?>
</body>
</html>
