<!DOCTYPE html>
<html>
<head>
	<title>Add Story</title>
    <meta charset="UTF-8">
</head>
<body>

<?php

session_start();
if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

if(isset($_POST['submit'])) {

if (!isset($_SESSION['username'])){
	echo "Error: Login before write new stories";
    echo "Redirect to homepage in 1 seconds...";
    header("Refresh:1; url = home.php");
	exit;
}

$mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');

if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

if($_POST['s_category'] == "") {
	$st_category = "Default";
}
else {
	$st_category = trim((string) $_POST['s_category']);
}

if($_POST['s_link'] == "") {
	$sl = "Original";
}
else {
	$sl = trim((string) $_POST['s_link']);
}

if($_POST['s_title'] == "" || $_POST['s_content'] == "") {
	echo "Title / Content cannot be empty...";
	header("Refresh:1; url = home.php");
	exit;
}

$sql = "insert into stories (category,s_title,s_content,link,username) values (?,?,?,?,?)";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param('sssss',htmlspecialchars($st_category),htmlspecialchars($_POST['s_title']),htmlspecialchars($_POST['s_content']),htmlspecialchars($sl),htmlspecialchars($_SESSION['username']));

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->execute();
echo "Story added...";
header("Refresh:1; url = home.php");
exit;
}
if(isset($_POST['discard'])) {
    header("Location:home.php");
    exit;
}
?>

<form id="st" method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" >
    <label for="s_t">Story title: </label><br>
    <textarea name="s_title" rows="2" cols="100" id="s_t"></textarea>
    <br>
    <label for="url">Link: </label><br>
    <textarea name="s_link" rows="2" cols="100" id="url">URL</textarea>
    <br>
    <label for="s_cate">Category: </label><br>
    <input type="text" name="s_category" id="s_cate" />
    <br>
    <label for="s_c">Story content: </label><br>
    <textarea name="s_content" rows="20" cols="100" id="s_c">Add your story content here.</textarea>
    <br>
	<input type="submit" name="submit" value="Submit" />
    <input type="submit" name="discard" value="Discard" />
	<input type="hidden" name="token" value="<?php echo $_POST['token'];?>" />
</form>

</body>
</html>
