#News site

* Team work of 451967 and 450061. 

URL: https://bitbucket.org/bofeiyu/spring2017-module3-451967-450061

URL: http://ec2-54-213-117-124.us-west2.compute.amazonaws.com/~bofei/home.php

URL: http://ec2-54-186-43-191.us-west-2.compute.amazonaws.com/~Bo/home.php

* Existed (usernames / password) are as following in bofei's website: byu / pass, bo / 123
* Existed (usernames/password) are as following in bo's website: abc / abcd

This is a news site contains follow functions: post stories, read stories, post comments read comments and manage stories & comments. 

The password is encrypted before storing into the database; Tokens have been added to the pages for adding, editing and deleting stories and comments. 

Database design: we have three tables in the 'm3' database. 

1. Users
	including information about individuals (username and password)
2. Stories
	using story id as primary key. 
	containing story title, content, category, link, issued time, author.
3. Comments
	using comment id as primary key.
	containing comment title, content, issued time, story and it's comments, author, and like count(creative portion).
	
When adding new stories or comments, the title and content cannot be empty; 
however, if the link left empty, we treated this as the article was written by the user, the link will be set as 'Original';
The category, if left empty, it would be filled with 'Default'.

Creative Portion: 

1. Add system timestamp for stories and comments
2. Registered user can 'like' the comment.
3. 'like' will be counted.