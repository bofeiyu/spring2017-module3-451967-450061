<!DOCTYPE html>
<html>
<head>
    <title>Read Story</title>
    <meta charset="UTF-8">
    <style>
        body {
            background-color: #DCDCDC;
            font-family: "Arial";
            font-size: 15px;
        }
        div.logbox {
            width: 100%;
            height: 60%;
            margin: 0px auto;
            border: 0px solid #;
            background-color: #FFFFFF;
        }
        div.innerbox h1 {
            background-color: #ADD8E6;
            padding: 10px;
            font-family: "Arial";
            font-weight: normal;
            color: black;
            border: 1px solid #98b9d0;
        }
        div fieldset {
            margin: 30px;
            border: 1px solid #98b9d0;
        }
    </style>
</head>
<body>
    <hr>
    <div class ="logbox">
        <div class = "innerbox">
        <h1>Welcome to BBS</h1>
        </div>

<?php
session_start();

$mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');
 
if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

if(isset($_SESSION['story_id'])) {
	$s_id=$_SESSION['story_id'];
} else {
	$s_id = $_GET['story_id'];	
}
$_SESSION['pass'] = $s_id;
$stmt = $mysqli->prepare("select * from stories where story_id=?");
$stmt->bind_param('s',$s_id);

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute(); 
$result = $stmt->get_result();

$story = $result->fetch_assoc();
    $_SESSION['s_category'] = $story['category'];
	$_SESSION['s_title'] = $story['s_title'];
	$_SESSION['s_link'] = $story['link'];
	$_SESSION['s_content'] = $story['s_content'];    
	$_SESSION['s_datetime']=$story['datetime'];
	$_SESSION['s_username']=$story['username'];
?>
    <div class="innerbox">
        <fieldset>
            <legend><label>Category:<?php echo $_SESSION['s_category']; ?></label></legend>
                <h1>Title:<?php echo $_SESSION['s_title']; ?></h1>
                Issued Time: <?php echo $_SESSION['s_datetime']; ?> <br/>
				Author: <?php echo $_SESSION['s_username']; ?> <br/>
				Link: <?php echo $_SESSION['s_link']; ?> <br/>
                <?php echo $_SESSION['s_content']; ?>
        </fieldset>
    </div>
<?php

$stmt = $mysqli->prepare("select * from comments where story_id=?");
$stmt->bind_param('s',$s_id);

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute(); 
$result = $stmt->get_result();

while ($comment = $result->fetch_assoc()){
    $_SESSION['c_id'] = $comment['comment_id'];
    $_SESSION['c_username'] = $comment['username'];
    $_SESSION['c_title'] = $comment['c_title'];
    $_SESSION['c_content'] = $comment['c_content'];
	$_SESSION['c_datetime'] = $comment['datetime'];
	$_SESSION['c_like'] = $comment['like_count'];
?>

<div class="innerbox">
        <fieldset>
            <legend>Comments: </legend>
                <br>Title: <?php echo $_SESSION['c_title']; ?> </br>
                <br>Issued Time: <?php echo $_SESSION['c_datetime']; ?> </br>
				<br>Author: <?php echo $_SESSION['c_username']; ?> </br>
				<br>Like: <?php echo $_SESSION['c_like']; ?></br>
				<form action="like.php",method="POST">
					<input type="submit" name="like" value="Like!">
					<input type='hidden' value="<?php echo $_SESSION['c_id']; ?>" name="comment_id">
				</form>
				</br>
				Content: </br>
                <?php echo $_SESSION['c_content']; ?>
        </fieldset>
    </div>
<?php
}
$stmt->close();
$_SESSION['token'] = substr(md5(rand()), 0, 10); // generate a 10-character random string
?>

<form id="return" method="POST" action="home.php" >
    <input type="submit" name="return" value="Return to homepage" />
</form>
<form id="new_comment" method="POST" action="add_comment">
    <input type="submit" name="comment" value="Write new comment" />
	<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
</form>
    </div>
</body>
</html>