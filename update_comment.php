<?php
if(isset($_POST['discard'])) {
    header("Location:view_sc.php");
    exit;
}
if(isset($_POST['submit'])) {
    session_start();
    $mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');
 
    if($mysqli->connect_errno) {
    	printf("Connection Failed: %s\n", $mysqli->connect_error);
    	exit;
    }

    $sql = "update comments set c_title=?,c_content=?,like_count=0 where comment_id=?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('ssi',$_POST['c_title'],$_POST['c_content'],$_SESSION['c_id']);

    if(!$stmt){
    	printf("Query Prep Failed: %s\n", $mysqli->error);
    	exit;
    }

    $stmt->execute();
    unset($_SESSION['c_id']);
    unset($_SESSION['c_title']);
    unset($_SESSION['c_content']);
    $stmt->close();
    
    echo "Comment edited...";
    header("Refresh:1; url = view_sc.php");
    exit;
}
?>