<!DOCTYPE html>
<html>
<head>
    <title>Stories and Comments</title>
    <meta charset="UTF-8">
    <style>
        body {
            background-color: #DCDCDC;
            font-family: "Arial";
            font-size: 15px;
        }
        div#logbox {
            width: 100%;
            height: 60%;
            margin: 0px auto;
            border: 0px solid #;
            background-color: #FFFFFF;
        }
        div#innerbox h1 {
            background-color: #ADD8E6;
            padding: 10px;
            font-family: "Arial";
            font-weight: normal;
            color: black;
            border: 1px solid #98b9d0;
        }
        div fieldset {
            margin: 30px;
            border: 1px solid #98b9d0;
        }
    </style>
</head>
<body>
    <hr>
    <div id ="logbox">
        <div id = "innerbox">
        <h1>Welcome to BBS</h1>
        </div>
<h1>Stories: </h1>
</div>
<?php
session_start();

if(isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
}
else {
    echo "Login first...";
    echo "Redirect to homepage in 1 seconds...";
    header("Refresh:1; url = home.php");
    exit;
}

$mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');
 
if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

$stmt = $mysqli->prepare("select * from stories where username=?");
$stmt->bind_param('s',$username);

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute(); 
$result = $stmt->get_result();

unset($_SESSION['token']);
$_SESSION['token'] = substr(md5(rand()), 0, 10); // generate a 10-character random string

while ($story = $result->fetch_assoc()){
    $_SESSION['s_id'] = $story['story_id'];
    $_SESSION['s_category'] = $story['category'];
	$_SESSION['s_title'] = $story['s_title'];
	$_SESSION['s_datetime']=$story['datetime'];
?>
    <div id="innerbox">
        <fieldset>
            <legend><label for="usertext">Category:<?php echo $_SESSION['s_category']; ?></label></legend>
                            <?php echo $_SESSION['s_title']; ?>
            <form action='read_story.php' method='GET'>
                <input type="submit" name="read" value="Read" />
                <input type='hidden' value="<?php echo $_SESSION['s_id']; ?>" name="story_id">
            </form>
            <form action='delete_story.php' method='POST'>
                <input type="submit" name="delete" value="Delete" />
                <input type='hidden' value="<?php echo $_SESSION['s_id']; ?>" name="story_id">
				<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            </form>
            <form action='edit_story.php' method='POST'>
                <input type="submit" name="edit" value="Edit" />
                <input type='hidden' value="<?php echo $_SESSION['s_id']; ?>" name="story_id">
				<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            </form>
        </fieldset>
    </div>
<?php
}
?>
<h1>Comments: </h1>
<?php
$stmt = $mysqli->prepare("select comment_id,story_id,c_title from comments where username=?");
$stmt->bind_param('s',$username);

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute(); 
$result = $stmt->get_result();

while ($comment = $result->fetch_assoc()){
    $_SESSION['c_id'] = $comment['comment_id'];
    $_SESSION['s_id'] = $comment['story_id'];	
    $_SESSION['c_title'] = $comment['c_title'];
?>
    <div id="innerbox">
        <fieldset>
            <legend><label for="usertext">Title:<?php echo $_SESSION['c_title']; ?></label></legend>
            <form action='read_story.php' method='GET'>
                <input type="submit" name="read" value="Read comment & Go to story" />
                <input type='hidden' value="<?php echo $_SESSION['s_id']; ?>" name="story_id">
            </form>
            <form action='delete_comment.php' method='POST'>
                <input type="submit" name="delete" value="Delete" />
                <input type='hidden' value="<?php echo $_SESSION['c_id']; ?>" name="comment_id">
				<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            </form>
            <form action='edit_comment.php' method='POST'>
                <input type="submit" name="edit" value="Edit" />
                <input type='hidden' value="<?php echo $_SESSION['c_id']; ?>" name="comment_id">
				<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>" />
            </form>
        </fieldset>
    </div>
<?php
}
$stmt->close();
?>

<form id="sc" method="POST" action="home.php" >
    <input type="submit" name="return" value="Return to homepage" />
</form>

</body>
</html>