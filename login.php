<!DOCTYPE html>
<html>
<head>
    <title>BBS Login</title>
    <meta charset="UTF-8">
</head>
<body>
<?php

function login() {
    $username=(string) trim($_POST['username']);
	$password=(string) trim($_POST['password']);

    $mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');

	if($mysqli->connect_errno) {
		printf("Connection Failed: %s\n", $mysqli->connect_error);
		exit;
	}

    $sql = 'select count(*), password from users where username=?';
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param('s',$username);

	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

	$stmt->execute();
	$stmt->bind_result($cnt, $pwd_hash);
	$stmt->fetch();
	if ($cnt==1 && password_verify($password,$pwd_hash)){
		$_SESSION['username']= $username;
	}
}

function register() {
    $username= (string) trim( $_POST['username']);
	$password= (string) trim( $_POST['password']);
	$encrypt = password_hash($password, PASSWORD_DEFAULT);

    $mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');

	if($mysqli->connect_errno) {
		printf("Connection Failed: %s\n", $mysqli->connect_error);
		exit;
	}

    $sql = 'select count(*) from users where username=?';
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param('s',$username);

		if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

	$stmt->execute();
	$stmt->bind_result($count);
	$stmt->fetch();
	$stmt->close();
	if ($count > 0){
		echo "User already existed, please change to another username...";
		echo "Redirect to homepage in 1 seconds...";
		header("Refresh:1; url = home.php");
		exit;
	} else {
		$sql = "insert into users(username, password) values (?,?)";
		$stmt = $mysqli->prepare($sql);
		$stmt->bind_param('ss',$username,$encrypt);
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}

		$stmt->execute();
		$_SESSION['username'] = $username;
	}
	$stmt->close();
}

session_start();
unset($_SESSION['username']);
if(isset($_POST['login'])){
    login();
    if(isset($_SESSION['username'])) {
        echo "Successfully Login...";
    } else {
        echo "Error: Login failed...";
    }
} else if(isset($_POST['register'])) {
    register();
    if(isset($_SESSION['username'])) {
        echo "Successfully Registered...";
    } else {
        echo "Error: Register failed...";
    }
} else {
    echo "Error: Invalid operation...";
    unset($_SESSION['username']);
    session_destroy();
}
    echo "Redirect to homepage in 1 seconds...";
    header("Refresh:1; url = home.php");
    exit;
?>
</body>
</html>
