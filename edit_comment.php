<!DOCTYPE html>
<html>
<head>
	<title>Edit Comment</title>
    <meta charset="UTF-8">
</head>
<body>

<?php
session_start();

if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

if (!isset($_SESSION['username'])){
    echo "Login first...";
    echo "Redirect to homepage in 1 seconds...";
    header("Refresh:1; url = home.php");
    exit;
}

$mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');

if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

$_SESSION['c_id'] = (string) trim($_POST['comment_id']);

$sql = "select * from comments where comment_id=?";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param('i',$_SESSION['c_id']);

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$result = $stmt->get_result();
$stmt->close();
$row = $result->fetch_assoc();
$_SESSION['c_title'] = $row['c_title']);
$_SESSION['c_content'] = $row['c_content']);
?>

<form id="st" method="POST" action="update_comment.php" >
    <label for="c_t">Comment title: </label><br>
    <textarea name="c_title" rows="2" cols="100" id="c_t"> <?php echo $_SESSION['c_title']; ?></textarea>
    <br>
    <label for="c_c">Comment content: </label><br>
    <textarea name="c_content" rows="20" cols="100" id="c_c"> <?php echo $_SESSION['c_content']; ?></textarea>
    <br>
	<input type="submit" name="submit" value="Submit" />
    <input type="submit" name="discard" value="Discard" />
</form>

</body>
</html>
