<!DOCTYPE html>
<html>
<head>
	<title>Add Comment</title>
    <meta charset="UTF-8">
</head>
<body>

<?php

session_start();

// Check token
if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

// Check if log in
if (!isset($_SESSION['username'])){
	echo "Error: Login before write new comments";
    echo "Redirect to homepage in 1 seconds...";
    header("Refresh:1; url = home.php");
	exit;
} else {
    $username = $_SESSION['username'];
}

if(isset($_POST['submit'])) {

// Get story id
$s_id = $_SESSION['pass'];

if($_POST['c_title'] == "" || $_POST['c_content'] == "") {
	echo "Title / Content cannot be empty...";
	header("Refresh:1; url = home.php");
	exit;
}

$mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');

// Connect to mysql
if($mysqli->connect_errno) {
	printf("Connection Failed: %s\n", $mysqli->connect_error);
	exit;
}

// Add comment to database
$sql = "insert into comments (story_id,username,c_title,c_content) values (?,?,?,?)";
$stmt = $mysqli->prepare($sql);
$stmt->bind_param('ssss',htmlspecialchars($s_id),htmlspecialchars($username),htmlspecialchars($_POST['c_title']),htmlspecialchars($_POST['c_content']));

if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
$stmt->execute();
echo "Comment added...";
header("Refresh:1; url = home.php");
exit;
}
if(isset($_POST['discard'])) {
    header("Location:home.php");
    exit;
}
?>

<form id="st" method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" >
    <label for="c_t">Comment title: </label><br>
    <textarea name="c_title" rows="2" cols="100" id="c_t"></textarea>
    <br>
    <label for="c_c">Comment content: </label><br>
    <textarea name="c_content" rows="20" cols="100" id="c_c">Add your comment content here.</textarea>
    <br>
	<input type="submit" name="submit" value="Submit" />
    <input type="submit" name="discard" value="Discard" />
	<input type="hidden" name="token" value="<?php echo $_POST['token'];?>" />
</form>

</body>
</html>
