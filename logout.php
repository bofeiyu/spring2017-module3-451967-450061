<?php
    session_start();
    unset($_SESSION['username']);
    session_destroy();
    echo "Succesfully logout...";
    echo "Redirect to homepage in 1 seconds...";
    header("Refresh:1; url = home.php");
    exit;
?>