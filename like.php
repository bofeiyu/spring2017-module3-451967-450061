<!DOCTYPE html>
<html>
<head>
    <title>Update Like</title>
    <meta charset="UTF-8">
</head>
<body>
<?php
    session_start();
    
    if(!isset($_SESSION['username'])) {
        echo "Login first...";
        echo "Redirect to homepage in 1 seconds...";
        header("Refresh:1; url = home.php");
        exit;
    }
    $mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');
 
    if($mysqli->connect_errno) {
    	printf("Connection Failed: %s\n", $mysqli->connect_error);
    	exit;
    }
    $c_id = $_GET['comment_id'];
    
    $sql = "select story_id,like_count from comments where comment_id=?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('i',$c_id);

    if(!$stmt){
    	printf("Query Prep Failed: %s\n", $mysqli->error);
    	exit;
    }

    $stmt->execute();    
    $result = $stmt->get_result();
    $res = $result->fetch_assoc();
    $_SESSION['story_id'] = $res['story_id'];
    $count=$res['like_count'] + 1;
    
    $sql = "update comments set like_count=? where comment_id=?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('si',$count,$c_id);

    if(!$stmt){
    	printf("Query Prep Failed: %s\n", $mysqli->error);
    	exit;
    }

    $stmt->execute();
    $stmt->close();
    header("Location:read_story.php");
    exit;
?>
</body>
</html>