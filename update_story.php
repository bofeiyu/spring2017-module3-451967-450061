<?php
if(isset($_POST['discard'])) {
    header("Location:view_sc.php");
    exit;
}
if(isset($_POST['submit'])) {
    session_start();
    $mysqli = new mysqli('localhost', 'bofei', 'pass', 'm3');
 
    if($mysqli->connect_errno) {
    	printf("Connection Failed: %s\n", $mysqli->connect_error);
    	exit;
    }

    $sql = "update stories set category=?,s_title=?,s_content=?,link=? where story_id=?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('ssssi',$_POST['s_category'],$_POST['s_title'],$_POST['s_content'],$_POST['s_link'],$_SESSION['story_id']);

    if(!$stmt){
    	printf("Query Prep Failed: %s\n", $mysqli->error);
    	exit;
    }

    $stmt->execute();
    unset($_SESSION['story_id']);
    unset($_SESSION['s_category']);
    unset($_SESSION['s_title']);
    unset($_SESSION['s_content']);
    unset($_SESSION['s_link']);
    $stmt->close();
    
    echo "Story edited...";
    header("Refresh:1; url = view_sc.php");
    exit;
}
?>